#renderizado de objetos

import pygame as pg
from settings import *

#clase principal
class ObjectRenderer:
    def __init__(self, game):
        self.game = game
        self.screen = game.screen
        self.wall_textures = self.load_wall_textures()
        self.sky_image = self.get_textures('resources/textures/sky.png', (WIDTH, HALF_HEIGHT))
        self.sky_offset = 0
        
    def draw(self):
        self.draw_background()
        self.render_game_objects()
        
#renderizar texturas del cielo y del piso
    def draw_background(self):
        #Sky
        self.sky_offset = (self.sky_offset + 4.0 * self.game.player.rel) % WIDTH
        self.screen.blit(self.sky_image, (-self.sky_offset, 0))
        self.screen.blit(self.sky_image, (-self.sky_offset + WIDTH, 0))
        #Floor
        pg.draw.rect(self.screen, FLOOR_COLOR, (0, HALF_HEIGHT, WIDTH, HEIGHT))
#metodo para renderizar objetos
    def render_game_objects(self):
        list_objects = sorted(self.game.raycasting.object_to_render, key=lambda t: t[0], reverse=True)
        for depth, image, pos in list_objects:
            self.screen.blit(image, pos)
    
#metodo estatico para traer las texturas
    @staticmethod
    def get_textures(path, res=(TEXTURE_SIZE, TEXTURE_SIZE)):
        texture = pg.image.load(path).convert()
        return pg.transform.scale(texture, res)


#metodo para cargar las texturas    
    def load_wall_textures(self):
        return {
            1: self.get_textures('resources/textures/1.png'),
            2: self.get_textures('resources/textures/2.png'),
            3: self.get_textures('resources/textures/3.png'),
            4: self.get_textures('resources/textures/4.png'),
            5: self.get_textures('resources/textures/5.png'),
            }
                     